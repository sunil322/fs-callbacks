const {createDirectory,createRandomJsonFiles,deleteJsonFiles} = require('../problem1.cjs');

createDirectory('JsonFolder',(error,data)=>{
    if(error){
        console.log(error);
    }else{
        createRandomJsonFiles(data,3,(error,data)=>{
            if(error){
                console.log(error);
            }else{
                deleteJsonFiles(data,(error,data)=>{
                    if(error){
                        console.log(error);
                    }else{
                        console.log(data);
                    }
                });
            }
        });
    }
});