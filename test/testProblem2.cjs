const { readingFile, writeUpperCaseData, writeLowerCaseData, writeSortedData, readFileDeleteListedFiles } = require('../problem2.cjs');

readingFile((error, data) => {
    if (error) {
        console.log(error);
    } else {
        writeUpperCaseData(data, (error) => {
            if (error) {
                console.log(error);
            } else {
                writeLowerCaseData((error) => {
                    if (error) {
                        console.log(error);
                    } else {
                        writeSortedData((error) => {
                            if (error) {
                                console.log(error);
                            } else {
                                readFileDeleteListedFiles((error, data) => {
                                    if (error) {
                                        console.log(error);
                                    } else {
                                        console.log(data);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});
