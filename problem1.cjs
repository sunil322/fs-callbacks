const fs = require('fs');
const path = require('path');

const createDirectory = (directoryName,callbackFunction) =>{
    const directory = path.join(__dirname,directoryName);

    fs.mkdir(directory,{recursive:true},(error)=>{
        if(error){
            callbackFunction(error);
        }else{
            if(typeof callbackFunction === 'function'){
                callbackFunction(null,directory);
            }
        }
    }); 
};

const createRandomJsonFiles = (directory,noOfFiles,callbackFunction) =>{
        
    function createFile(){
        if(noOfFiles>=1){
            let randomFileName = Math.random().toString().slice(2) + '.json';
            const jsonFilePath = path.join(directory,randomFileName);
            fs.open(jsonFilePath,'w',(error)=>{
                if(error){
                    callbackFunction(error)
                }else{
                    console.log(randomFileName+" file created");
                    noOfFiles--;
                    createFile();
                    if(noOfFiles===0){
                        callbackFunction(null,directory);
                    }
                }
            });

        }
    }
    createFile(); 
};

const deleteJsonFiles = (directory,callbackFunction) =>{

    fs.readdir(directory,(error,data)=>{
        if(error){
            callbackFunction(error);
        }else{
            let noOfFilesPresent = data.length;
            
            function deleteFile(){
                if(noOfFilesPresent >= 1){
                    const fileName = data[noOfFilesPresent-1];
                    const filePath = path.join(directory,fileName);
                    fs.unlink(filePath,(error)=>{
                        if(error){
                            callbackFunction(error);
                        }else{
                            console.log(fileName +" Deleted");
                            noOfFilesPresent--;
                            deleteFile();
                            if(noOfFilesPresent === 0){
                                callbackFunction(null,"All Task Finished");
                            }
                        }
                    });
                }
            }
            deleteFile();
        }
    });
};

module.exports = {createDirectory,createRandomJsonFiles,deleteJsonFiles}; 