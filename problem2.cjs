const fs = require('fs');
const path = require('path');

//Reading lipsum.txt file
const readingFile = (callback) => {
    fs.readFile('lipsum.txt', 'utf8', (error, data) => {
        if (error) {
            callback(error);
        } else {
            callback(null, data);
        }
    });
};

//Writing upper case data to a file and storing that file name in filenames.txt
const writeUpperCaseData = (data, callback) => {

    const upperCaseFilePath = path.join(__dirname, 'lipsumUpper.txt');
    const fileName = path.join(__dirname, 'filenames.txt');

    let dataInUpperCase = data.toUpperCase();

    fs.writeFile(upperCaseFilePath, dataInUpperCase, (error) => {
        if (error) {
            callback(error);
        } else {
            fs.writeFile(fileName, upperCaseFilePath, (error) => {
                if (error) {
                    callback(error);
                } else {
                    callback();
                }
            });
        }
    });
};

//Writing lower case data to a file and storing that file name in filenames.txt
const writeLowerCaseData = (callback) => {

    const upperCaseFilePath = path.join(__dirname, 'lipsumUpper.txt');

    fs.readFile(upperCaseFilePath, 'utf8', (error, data) => {
        if (error) {
            callback(error);
        } else {
            const lowerCaseFilePath = path.join(__dirname, 'lipsumLower.txt');
            const fileName = path.join(__dirname, 'filenames.txt');

            let dataInLowerCase = data.toLowerCase();
            let dataInSentences = dataInLowerCase.split('.').join('\n');

            fs.writeFile(lowerCaseFilePath, dataInSentences, (error) => {
                if (error) {
                    callback(error);
                } else {
                    fs.writeFile(fileName, '\n' + lowerCaseFilePath, { flag: 'a' }, (error) => {
                        if (error) {
                            callback(error);
                        } else {
                            callback();
                        }
                    });
                }
            });
        }
    });
};

//Writing sorted data to a file and storing that file name in filenames.txt
const writeSortedData = (callback) => {

    const upperCaseFilePath = path.join(__dirname, 'lipsumUpper.txt');
    const lowerCaseFilePath = path.join(__dirname, 'lipsumLower.txt');

    //Reading two files first lipsumUpper.txt then lipsumLower.txt 
    fs.readFile(upperCaseFilePath, 'utf8', (error, dataInUpper) => {
        if (error) {
            callback("Error occured during reading file");
        } else {
            fs.readFile(lowerCaseFilePath, 'utf8', (error, dataInLower) => {

                const sortedDataFilePath = path.join(__dirname, 'lipsumSorted.txt');
                const filesName = path.join(__dirname, 'filenames.txt');

                let totalData = dataInLower + dataInUpper;
                let sortedData = totalData.split(' ').sort().join('\n');

                fs.writeFile(sortedDataFilePath, sortedData, (error) => {
                    if (error) {
                        callback(error);
                    } else {
                        fs.writeFile(filesName, '\n' + sortedDataFilePath, { flag: 'a' }, (error) => {
                            if (error) {
                                callback(error);
                            } else {
                                callback();
                            }
                        });
                    }
                });
            });
        }
    });
};

//Delete all files listed in the filenames.txt
const readFileDeleteListedFiles = (callback) => {

    const fileName = path.join(__dirname, 'filenames.txt');

    fs.readFile(fileName, 'utf8', (error, data) => {
        if (error) {
            callback(error);
        } else {

            let noOfFilesPresent = data.split('\n').length;
            let filesList = data.split('\n');

            function deleteFile() {
                if (noOfFilesPresent >= 1) {
                    let filePath = filesList[noOfFilesPresent - 1];
                    fs.unlink(filePath, (error) => {
                        if (error) {
                            callback(error);
                        } else {
                            noOfFilesPresent--;
                            deleteFile();
                            if (noOfFilesPresent === 0) {
                                callback(null, "All files deleted");
                            }
                        }
                    });
                }
            }
            deleteFile();
        }
    });
};

module.exports = { readingFile, writeUpperCaseData, writeLowerCaseData, writeSortedData, readFileDeleteListedFiles };